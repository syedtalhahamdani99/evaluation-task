import {Component, Inject} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-editdialog',
  templateUrl: './editdialog.component.html',
  styleUrls: ['./editdialog.component.css']
})
export class EditdialogComponent {
  editForm: FormGroup;

  constructor(
    private dialogRef: MatDialogRef<EditdialogComponent>,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    console.log('Received Data in EditdialogComponent:', data);
        this.editForm = this.fb.group({
        name: [data.product.name, Validators.required],
        price: [data.product.price, Validators.required],
        category: [data.product.category, Validators.required],
      });
        console.log(this.editForm.value)
  }
  saveChanges(): void {
    this.dialogRef.close(this.editForm.value);
  }
  closeDialog(): void {
    this.dialogRef.close();
  }
}
