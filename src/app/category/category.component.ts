import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CategoryService} from "../services/category.service";
import {AngularFirestore} from "@angular/fire/compat/firestore";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  categoryForm: FormGroup;

  constructor(private snackBar: MatSnackBar,private fireStore: AngularFirestore, private fb: FormBuilder, private categoryService: CategoryService) {
  }

  get catNameControl() {
    return this.categoryForm.get('catName');
  }

  ngOnInit(): void {
    this.categoryForm = this.fb.group({
      catName: ['', Validators.required]
    });
  }

  onSubmit(): void {
    if (this.categoryForm.valid) {
      const catName = this.catNameControl.value;

      this.fireStore.collection("categories").add({cat: catName})
          .then((docRef) => {
            console.log("Document ref with id ", docRef.id);
            this.showSuccessSnackbar('Category Added Successfully');

            docRef.update({id: docRef.id});
          })
          .catch((error) => {
            console.error("Error adding document ", error);
          });

      this.categoryForm.reset();
    }
  }
  showSuccessSnackbar(message: string): void {
    this.snackBar.open(message, 'Close', {
      duration: 1000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }
}
